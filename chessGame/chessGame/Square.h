#pragma once
#include <string>




class Square
{

public:
	
	//Constructors
	Square();
	Square(const std::string& pos);
	Square(const int& x, const int& y);
	~Square() = default;
	

	//Getters
	int getX() const;
	int getY() const;

	//Setters
	void setPosition(const int& x, const int& y);
	void setX(const int& newX);
	void setY(const int& newY);
	
	//Methods
	double Calc_Distance(const Square& other) const;
	static Square empty();

	//Operators
	bool operator!=(const Square& other) const;
	bool operator==(const Square& other) const;

private:

	//Fields
	int _x;
	int _y;

};

