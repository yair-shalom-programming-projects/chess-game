#include "Knight.h"


							     

//? Constructors ?//


// -- C'tor -- //
Knight::Knight(const char& type) :
	Piece(type)
{
}



//? Methods ?//

/*
	-- check if Knight's move is valid --
	* input: current Square, dest Square, board
	* output: bool - valid move or not
*/
bool Knight::isMoveValid(const Square& curSqr, const Square& destSqr, 
	const Board& board)
{
	/*
		-- KNIGHT MOVE RULES --
		1. two step in a line and another one diagonally 
	*/

	// the distance between Knight's src sqrt to his dest sqrt is 
	// static and equals to VALID_DISTANCE
	double distance = curSqr.Calc_Distance(destSqr);
	

	if (distance == KNIGHT_STEP)
		return true;
	return false;
}

