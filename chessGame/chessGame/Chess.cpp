#include "Chess.h"
#include "Pipe.h"
#include "ChessUtils.h"

#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"
#include "Pawn.h"


using namespace ChessUtils;

using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::map;
using std::array;




//? Constructors ?//

/* -- C'tor -- */
Chess::Chess() :
	_board(boardStruct{ {

			//black pieces
			{
				new Rook('r'), new Knight('n'), new Bishop('b'), new King('k'),
				new Queen('q'), new Bishop('b'), new Knight('n'), new Rook('r')
			},
			{
				new Pawn('p'), new Pawn('p'), new Pawn('p'), new Pawn('p'),
				new Pawn('p'), new Pawn('p'), new Pawn('p'), new Pawn('p')
			},


		// Empty Squares
		{ nullptr, nullptr, nullptr, nullptr,
		nullptr, nullptr, nullptr, nullptr },
		{ nullptr, nullptr, nullptr, nullptr,
		nullptr, nullptr, nullptr, nullptr },
		{ nullptr, nullptr, nullptr, nullptr,
		nullptr, nullptr, nullptr, nullptr },
		{ nullptr, nullptr, nullptr, nullptr,
		nullptr, nullptr, nullptr, nullptr },


		//white pieces
		{
			new Pawn('P'), new Pawn('P'), new Pawn('P'), new Pawn('P'),
			new Pawn('P'), new Pawn('P'), new Pawn('P'), new Pawn('P')
		},
		{
			new Rook('R'), new Knight('N'), new Bishop('B'),new King('K'),
			new Queen('Q'), new Bishop('B'), new Knight('N'), new Rook('R')
		}
	} })
{
}


//? Methods ?//


/* ~ play the Chess game ~ */
void Chess::play()
{
	

	char msgToGraphics[MAX_MSG_SIZE];
	responseCode resCode = responseCode::NONE;
	string msgFromGraphics;



	//connecting to graphics
	connect();
	cout << "Connected!\n" << endl;


	//send First message [board first state (includes how starts)]
	strcpy_s(msgToGraphics, BOARD_FIRST_STATE);
	_p.sendMessageToGraphics(msgToGraphics);
	


	while (true)
	{
		//print Board current state
		std::cout << "\nBoard State ~ " << endl;
		_board.show();



		// get movement (ex: e1b3 => e1 = src square, b3 = dst square)
		msgFromGraphics = _p.getMessageFromGraphics();
		
		if (msgFromGraphics == "quit")
			break;
		
		else if (msgFromGraphics == "restart")
			restartGame();
		
		else
			handleTurn(msgFromGraphics, resCode);

		
		//create propriate message to graphics ( board + response code )
		strcpy_s(msgToGraphics, prepareMsgToGraphics(resCode).c_str());
		_p.sendMessageToGraphics(msgToGraphics);


	

		system("cls");
	}

	//close pipe
	_p.close();

}
/*
	-- connecting to Graphics --
	* output: connected or exit
*/
bool Chess::connect()
{

	cout << "trying connect.." << endl;

	while (true)
	{
		if (_p.connect())
			return true;
		Sleep(3000);
	}
	system("cls");
}

std::string Chess::prepareMsgToGraphics(const responseCode& resCode)
{

	string msg = "";
	Square currSqr;

	for (int y = 0; y < BOARD_SIDE; ++y)
		for (int x = 0; x < BOARD_SIDE; ++x)
		{
			currSqr.setPosition(x, y);
			msg += _board.getSqrType(currSqr, true);
		}

	msg += std::to_string(int(resCode));

	return msg;
}




/*
	-- handle current player turn - find if a move is valid, then make 
	the move and switch the turn --
	
	* input: piece's current Square, dest Square (combined in string) 
	* output: response code

*/
bool Chess::handleTurn(const string& movement, responseCode& resCode)
{


	// convert movement to src and dest Squares
	Square src(movement.substr(0, 2));
	Square dest(movement.substr(2, 2));




	// find if move is NOT valid
	validMove(src, dest, resCode);

	if ( resCode != responseCode::NONE)
		return false;


	// if the src eats the dest
	if (_board.getSqr(dest) && _board.getPieceColor(dest) != _turn)
		++_eatCounter;

	// MOVE PIECE  
	_board.setMove(src, dest);


	// promote pawn (if exist)
	pawnPromotion(dest);


	// set all Blockable Piece's threaten Squares
	setAllThreatenSqrs(_board);
	
	


	// find if check. if so, find if checkmate (there's no checkmate without check)
	if ( move::isCheck(src, dest, _board) )

		resCode = move::checkMate(dest, _board) ?
						responseCode::CHECKMATE : responseCode::CHECK;
	else
	{
		_isCheck = false;

		// check for a draw
		if (move::isDraw( *_board.getSqr(dest) , _board ))
			resCode = responseCode::DRAW;

	}

	// if it's got here and resCode is NONE, the move is valid & regular
	if (resCode == responseCode::NONE)
		resCode = responseCode::REGULAR;


	// switch the Turn to the other player
	switchTurn();

	//update first move and En Passant 
	_board.getSqr(dest)->updateFirstMove();
	updateEnPassant(dest, _board);


	return true;

}




/* 
	-- check if the move is valid -- 
	* input: src & dest Square, rCode ref
	* output: n/a
*/
void Chess::validMove(const Square& src, const Square& dest,
	responseCode& resCode)
{

	typedef bool (*funcType)(const Square&, const Square&, const Board&);


	
	// array of maps of all the validation functions & responseCode 
	// (using array of maps instead of map because a specific order requires)
	array <map <responseCode, funcType>, 6> validators = {{

		{{ responseCode::BAD_RANGE,      &valid::squaresInBoard    }},
		{{ responseCode::EQUAL_SQUARES,  &valid::notEqualSquares   }},
		{{ responseCode::BAD_SRC,        &valid::playerSrcSquare   }},
		{{ responseCode::BAD_DEST,       &valid::playerDestSquare  }},
		{{ responseCode::BAD_MOVE ,      &valid::badPieceMove      }},
		{{ responseCode::SELF_CHECK,     &valid::notSelfCheck      }}

	}};



	// find if the move isn't valid if so, return response code error
	for (const auto& m : validators)
	{
		for (const auto& validator : m)
		{
			// calling a specific func to find if a specific aspect is valid  
			if (!validator.second(src, dest, _board))
			{
				resCode = validator.first;
				return;
			}
		}
	}


	// find it's gets here, the move is valid
	resCode = responseCode::NONE;

}




/*
	-- if a pawn reached the other side of the board, 
	he gets promotion to Queen --
	
	* input: dest square 
*/
void Chess::pawnPromotion(const Square& dest)
{

	if ( tolower(_board.getSqrType(dest)) == 'p' )
	{

		int lastY = _turn == color::black ? BOARD_SIDE - 1 : 0;
		int type = _turn == color::black ? 'q' : 'Q';

		// if pawn reached the other side of the board - pawn gets promotion
		if (dest.getY() == lastY)
			//switch piece from Pawn to Queen
			_board.setPiece(dest, new Queen(type));

	}
}

/* --  -- */
void Chess::restartGame()
{

	_board.setBoard({ {

			//black pieces
			{
				new Rook('r'), new Knight('n'), new Bishop('b'), new King('k'),
				new Queen('q'), new Bishop('b'), new Knight('n'), new Rook('r')
			},
			{
				new Pawn('p'), new Pawn('p'), new Pawn('p'), new Pawn('p'),
				new Pawn('p'), new Pawn('p'), new Pawn('p'), new Pawn('p')
			},


		// Empty Squares
		{ nullptr, nullptr, nullptr, nullptr,
		nullptr, nullptr, nullptr, nullptr },
		{ nullptr, nullptr, nullptr, nullptr,
		nullptr, nullptr, nullptr, nullptr },
		{ nullptr, nullptr, nullptr, nullptr,
		nullptr, nullptr, nullptr, nullptr },
		{ nullptr, nullptr, nullptr, nullptr,
		nullptr, nullptr, nullptr, nullptr },


		//white pieces
		{
			new Pawn('P'), new Pawn('P'), new Pawn('P'), new Pawn('P'),
			new Pawn('P'), new Pawn('P'), new Pawn('P'), new Pawn('P')
		},
		{
			new Rook('R'), new Knight('N'), new Bishop('B'),new King('K'),
			new Queen('Q'), new Bishop('B'), new Knight('N'), new Rook('R')
		}
	} });

	_turn = color::white;
	_enPassantSqr = Square::empty();


}




/* -- set all pieces' threaten squares -- 
	*input: the board
*/
void Chess::setAllThreatenSqrs(const Board& board)
{
	Piece* currPiece;
	Square currSqr;

	
	//loop over board to find all Blockable Pieces
	for (int y = 0; y < BOARD_SIDE; ++y)
		for (int x = 0; x < BOARD_SIDE; ++x)
		{
			
			currSqr.setPosition(x, y);
			currPiece = board.getSqr(currSqr);

			if (currPiece && dynamic_cast<Blockable*>(currPiece))
				currPiece->setThreatenSquares(currSqr, board);
			
				

		}

}
