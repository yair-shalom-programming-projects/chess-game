#include "Rook.h"



//? Cnstructors ?//

/* -- C'tor -- */
Rook::Rook(const char& type) :
	Blockable(type)
{
}


//? Getters ?//

/* -- is first move -- */
bool Rook::isFirstMove() const
{
	return _firstMove;
}


//? Methods ?//

/*
	-- check if Rook's move is valid --
	* input: current Square, dest Square, board
	* output: bool - valid move or not
*/
bool Rook::isMoveValid(const Square& curSqr, const Square& destSqr,
													const Board& board)
{
	/*
		-- ROOK MOVE RULES --
		1. move only in a line
	*/

	// Find if the Rook is moving in a line and if his path is clear
	for (int i = 0; i < _threatenSquares.size(); ++i)
	{
		if (destSqr == _threatenSquares[i])
		{
			return true;
		}
	}

	return false;
}



/*
	-- Sets the squares that the rook threates--
	* input: current Square,  board
*/
void Rook::setThreatenSquares(const Square& currSqr, const Board& board)
{
	_threatenSquares.resize(0);


	checkBlocked(currSqr, 1, 0, board);
	checkBlocked(currSqr, -1, 0, board);
	checkBlocked(currSqr, 0, 1, board);
	checkBlocked(currSqr, 0, -1, board);

}
