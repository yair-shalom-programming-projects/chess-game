#pragma once

#include "Square.h"
#include "Piece.h"
#include "Board.h"



class Pawn : public Piece
{


public:
	
	//constructors
	Pawn(const char& type);
	virtual ~Pawn() = default;






	//Methods
	virtual bool isMoveValid(const Square& curSqr,
		const Square& destSqr, const Board& board) override;
	
	bool isEatValid(const Square& currSqr, const Square& destSqr, 
		const Board& board) const;
	
	
	



};

