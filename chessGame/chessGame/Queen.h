#pragma once


#include "Blockable.h"


class Queen: public Blockable
{

public:


	// constructors
	Queen(const char& type);
	virtual ~Queen() = default;
	

	// methods
	virtual bool isMoveValid(const Square& curSqr,
								const Square& destSqr, const Board& board) override;
	virtual void setThreatenSquares(const Square& currSqr, const Board& board);


	
};

