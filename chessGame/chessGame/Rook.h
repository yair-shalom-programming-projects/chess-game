#pragma once

#include "Blockable.h"


class Rook : 
	public Blockable
{


public:

	//Constructors
	Rook(const char& type);
	virtual ~Rook() = default;

	//getters
	bool isFirstMove() const;


	//Methods
	virtual bool isMoveValid(const Square& curSqr, const Square& destSqr,
		const Board& board) override;
	virtual void setThreatenSquares(const Square& currSqr, const Board& board);

};