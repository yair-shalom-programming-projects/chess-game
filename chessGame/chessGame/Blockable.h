#pragma once
#include "Piece.h"
#include "Square.h"
#include "Board.h" 

class Blockable : public Piece
{
public:

	//constructors
	Blockable(const char& type);
	virtual ~Blockable() = default;

	//Getters
	virtual Square getThreatendSqr(int index) const;

	//Setters
	virtual  void setThreatenSquares(const Square& currSqr,
		const Board& board) = 0;


	//Methods
	virtual bool isMoveValid(const Square& curSqr,
		const Square& destSqr, const Board& board) = 0;
	
protected:
	virtual void checkBlocked(const Square& srcSqr, int adderX, int adderY,
		const Board& board);

	std::vector<Square> _threatenSquares;

};

