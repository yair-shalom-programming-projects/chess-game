#include "Blockable.h"



//? C'tor ?// 
Blockable::Blockable(const char& type) :
	Piece(type)
{

}

//? Getters //?
/*
	--Get a square from the threatened squares vector--
	*input : the index of the square
	*output: the Square , if index out of range it returns
			a square with values of -1 , -1
*/
Square Blockable::getThreatendSqr(int index) const
{
	if (index >= 0 && index < _threatenSquares.size())

		return _threatenSquares[index];
	
	return Square::empty();
}



//? Setters ?//

/*
	--  checks when the piece movement is blocked
		and adds to a vector the squares that are in the path --
	* input: source square
			 the amount to add to src x
			 the amount to add to src y
			 board
	* output: valid move or not
*/
void Blockable::checkBlocked(const Square& srcSqr, int adderX, int adderY,
	const Board& board)
{

	int srcX = srcSqr.getX() + adderX, srcY = srcSqr.getY() + adderY;
	Square currSquare(srcX, srcY);


	try
	{
		// looping until incountering not an empty square. note: 
		// an exception will be thrown if currSquare isn't in Board bounds
		while ( !board.getSqr(currSquare) )
		{

			//checks if the sqr is in bounds
			board.getSqr(currSquare);
			_threatenSquares.push_back(currSquare);
			srcY += adderY;
			srcX += adderX;
			currSquare.setPosition(srcX, srcY);
		}
	}	
	catch (...)
	{
		return;
	}



	//find if they aren't from the same team;
	if (board.getPieceColor(srcSqr) != board.getPieceColor(currSquare))

		//if so, add currSquare to threaten Square (cause src can eat curr)
		_threatenSquares.push_back(currSquare);

}

