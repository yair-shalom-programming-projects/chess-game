#include "ChessUtils.h"
#include "Chess.h"

#include "Pawn.h"
#include "King.h"
#include "Rook.h"

using namespace ChessUtils;

using std::string;







/* 
	-- Update the en Passant -- 
	* input: dest Square, board
*/
void ChessUtils::updateEnPassant(const Square& dest, const Board& board)
{
	

	if ( _enPassantSqr != Square::empty() )
	{

		Piece* enPassant = board.getSqr(_enPassantSqr);

		//if piece exist or has been eaten 
		if ( enPassant )
		{
			// if full round passed
			if ( board.getPieceColor(_enPassantSqr) == _turn )
				_enPassantSqr = Square::empty();
		}

	}

	/* it's ok to reset the enPassant Sqr to 0,0 (using reset func) cause the
	 square will NEVER be 0,0  */
}



/*	-- return the other player's turn -- */
color ChessUtils::otherTurn()
{
	return _turn == color::black ? color::white : color::black;
}
/* -- switch the _turn to the other player -- */
void ChessUtils::switchTurn()
{
	_turn = otherTurn();
}







//?  Valid namespace Methods  ?//


/*
	-- checks if a move is in board borders --
	* input: source square, dest square , board
	* output : if a move is in board borders or not
*/
bool valid::squaresInBoard(const Square& src, const Square& dest,
	const Board& board)
{

	try
	{

		// an exception will be throw if the squares aren't in Board's bounds
		board.getSqr(src);
		board.getSqr(dest);

		return true;
	
	}
	catch (...)
	{ return false; }
	
}


/*
	-- checks if the dest square is src square too --
	* input: source square, dest square , board
	* output : squares are the same or not
*/
bool valid::notEqualSquares(const Square& src, const Square& dest,
	const Board& board)
{
	return src != dest;
}


/*
	-- Checks if in the src square has the the right color piece --
	* input: src square, dest square and the board
	* output : if it has the right piece or not
*/
bool valid::playerSrcSquare(const Square& src, const Square& dest,
	const Board& board)
{
	return board.getPieceColor(src) == _turn;
}


/*
	-- checks if the dest square is valid - dest square is empty or if it 
	isn't, it should be castling ( src=king, dest=rook ) --
	* input: source square, dest square, board
	* output : true if not occupied false if is
*/
bool valid::playerDestSquare(const Square& src, const Square& dest,
	const Board& board)
{



	// if dest isn't empty and the dest is the current player's piece 
	if ( board.getPieceColor(dest) == _turn )
	{
		
		// this state can only exist if the src os king and dest is rook (castling)
		return tolower(board.getSqrType(src)) == 'k' &&
			tolower(board.getSqrType(dest)) == 'r';
	
	}
	return true;

}


/*
	--  checks if the piece movement is valid --
	* input: source square, dest square, board
	* output: valid move or not
*/
bool valid::badPieceMove(const Square& src, const Square& dest, 
	const Board& board)
{
	return board.getSqr(src)->isMoveValid(src, dest, board);
}


/*
	--  checks if the move will make a self check --
	* input: src square, dest square, board
	* output: is it a self check or not
*/
bool valid::notSelfCheck(const Square& src, const Square& dest,
	const Board& constBoard )
{
	
	// a change to the board is necessary. and for not ruin the amazing maps
	// implemeted in Chess::validMove, a const board will be transfer as param.
	// the solution used here is to const cast the board param
	Board& board = const_cast<Board&>(constBoard);


	
	// simulate the current move
	Piece* helper = board.getSqr(dest);

	board.setMove(src, dest, false);


	// switch the turn to simulate the other player next move
	switchTurn();
	// the "new" move requires set all threaten sqrs
	Chess::setAllThreatenSqrs(board);



	// find if there would be a check if the move will executed
	bool isSelfCheck = move::isCheck(src, dest, board);



	// discarding move
	board.setMove(dest, src, false);
	board.setPiece(dest, helper);


	// set em' back 
	Chess::setAllThreatenSqrs(board);
	// switch back the turn current player
	switchTurn();



	// if NOT self Check
	return !isSelfCheck;

}







//?  Move namespace Methods  ?//

/*
	 -- checks if a move makes a checks on enemy king --
	* input: source square, dest square , board, self (check)
	* output: is it an enemy check or not
*/
bool move::isCheck(const Square& src, const Square& dest, const Board& board)
{

	Piece* currPiece;

	Square currSqr;
	Square kingSqr;


	char kingType;
	char currType;




	// getting the enemy king square & type  
	kingSqr = move::getKingSquare(board, otherTurn());
	kingType = board.getSqrType(kingSqr);
	




	//running over the Board to see if there's enemy threat on the king
	for (int y = 0; y < BOARD_SIDE; ++y)
		for (int x = 0; x < BOARD_SIDE; ++x)
		{

			currSqr.setPosition(x, y);

			// check if the square if not empty (nullptr)
			if (currPiece = board.getSqr(currSqr))
			{

				currType = currPiece->getType();

				// if King & current Piece are NOT from the same team
				if (islower(currType) != islower(kingType))
				
					// and if current Piece is valid - it's a check!
					if (currPiece->isMoveValid(currSqr, kingSqr, board))
					{
						_isCheck = true;
						return true;
					}
			}
		}




	return false;

}

/*
	-- Checks if the move made a checkmate on the opponent
	*input:  source square, dest square , board
	*output: checkmate or not

*/
bool move::checkMate( const Square& dest, 
	const Board& board)
{
	/*
		CONDITIONS FOR A CHECKMATE:
		1. there's a check
		2. The king has nowhere to move
		3.The piece who checks can't be eaten
		4.The piece who checks' path can't be blocked
	*/

	bool isCheckmate = false;


	// changing turn state
	switchTurn();

	// get king Square. (need the other player's king)
	Square king = move::getKingSquare(board, _turn);

	
	if (!canKingMove(king, board) && !isCheckBlockable(king, dest, board))
		isCheckmate = true;

	
	switchTurn();

	return isCheckmate;
	
}

/*
	-- Checks if there is a draw --
	* input: piece, board
	* output: bool - is draw
*/
bool move::isDraw(const Piece& piece, const Board& board)
{

	bool isDraw = false;
	
	/* 
		DRAW CONDITIONS: 
		- No move can be made 
		OR
		- Not enough material to deliver a checkmate
	*/

	switchTurn();

	if (!availableMove(board) || insufficientMaterial(piece ,board))
		isDraw = true;

	switchTurn();
	
	return isDraw;

}



//? helper functions ?//


/*
	-- find if king can move  (helper func to checkmate and isDraw) --
	* input: king's sqr, board
	* output: can espace or not
*/
bool move::canKingMove(const Square& king, const Board& board)
{

	Square currSqr;
	color currColor;


	for (int y = king.getY() - 1; y <= king.getY() + 1; ++y)
		for (int x = king.getX() - 1; x <= king.getX() + 1; ++x)
		{

			currSqr.setPosition(x, y);

			try
			{
				currColor = board.getPieceColor(currSqr);
			}
			// catch if the currSqr isn't in board bounds 
			catch (...)
			{
				continue;
			}


			// Check if the Square is not filled with
			// the same color piece as the enemy king
			if (currColor != _turn)

				if (validMovement(king, currSqr, board))
					return true;
		}

	return false;

}
/*
	-- find if it is possible to block the checker (helper to checkmate) --
	* input: king sqr, dest sqr, board
	* output: is it possible to block the checker or not
*/
bool move::isCheckBlockable(const Square& king, const Square& dest,
	const Board& board)
{

	Piece* checker = board.getSqr(dest);
	Piece* curr;

	Square currSqr;
	Square threatenSqr = dest;

	int i = 0;


	for (int y = 0; y < BOARD_SIDE; ++y)
		for (int x = 0; x < BOARD_SIDE; ++x)
		{

			currSqr.setPosition(x, y);
			curr = board.getSqr(currSqr);


			if (curr)
			{

				// if current Piece is an ally
				if (board.getPieceColor(currSqr) != _turn)
				{
					// if there's another ally piece who checks the king
					// & the king has nowhere to move - it's a checkmate!
					if (curr->isMoveValid(currSqr, king, board) && curr != checker)
						return false;
				}

				// if current Piece is an enemy
				else
				{

					// find if the checker can be eaten or can be blocked
					// if threatenSqr x,y are -1,-1 then it looped over all the
					// threatened square vector

					i = 0;
					while (threatenSqr != Square::empty())
					{
						if (validMovement(currSqr, threatenSqr, board))
							return true;
						++i;
						threatenSqr = checker->getThreatendSqr(i);
					}
					threatenSqr = dest;
				}
			}

		}
	return false;
}
/*
	-- find if the player's have any availble move to make --
	* input: board
	* output: bool - true or false
*/
bool move::availableMove(const Board& board)
{

	Square kingSqr = move::getKingSquare(board, _turn);

	//check if a king can move
	if ( canKingMove(kingSqr, board) )
		return true;
	

	Piece* currPiece;
	Square pieceSqr;
	Square currSqr;

	int i = 0, j = 0;



	for (int y = 0; y < BOARD_SIDE; ++y)
		for (int x = 0; x < BOARD_SIDE; ++x)
		{

			pieceSqr.setPosition(x, y);

			// skip king (already checked)
			if ( pieceSqr == kingSqr )
				continue;


			// checks if there is a piece and its an enemy piece
			if (board.getPieceColor(pieceSqr) == _turn)
			{

				currPiece = board.getSqr(pieceSqr);


				// checking if the piece is blockable 
				if (dynamic_cast<Blockable*>(currPiece))
				{
					// check if current piece has where to go 
					if (currPiece->getThreatendSqr(1) != Square::empty())
						return true;
				}

				else
				{
					// find all possible moves in 2 sqr radius (all options) 
					for (i = y - 2; i <= y + 2; ++i)
						for (j = x - 2; j <= x + 2; ++j)
						{

							currSqr.setPosition(j, i);

							try
							{
								// find if in current sqr there's ally piece 
								if (board.getPieceColor(currSqr) != _turn)

									// and if the current enemy piece can eat him
									if (validMovement(pieceSqr, currSqr, board))
										return true;
							}
							catch (...)
							{
								// catch not in board bounds
							}
						}

				}
			}
			
		}
	return false;

}
/*
	-- find if there is a case of insufficient material --
	* input: piece, board
	* output: true or false
*/
bool move::insufficientMaterial(const Piece& piece, const Board& board)
{

	Square currSqr;

	char type = tolower(piece.getType());


	// insufficientMaterial can only happen with 4 pieces on the board
	if (_eatCounter < 28)
		return false;

	// checks if the piece is not a bishop or a knight
	if (type != 'n' && type != 'b')
		return false;


	for (int x = 0; x < BOARD_SIDE; ++x)
		for (int y = 0; y < BOARD_SIDE; ++y)
		{

			currSqr.setPosition(x, y);

			// checks it's not the the piece that did the move
			if ((type = board.getSqrType(currSqr)) && type != piece.getType())
			{

				type = tolower(type);

				// if its not the king
				if (type != 'k')
				{
					// checks that its not one of the kings
					if (

						// checks if its not a bishop or knight
						(type != 'n' && type != 'b') ||

						// if the only other material left is the same color
						// as the second one that left than its not insufficient material
						board.getPieceColor(currSqr) != _turn

						)

						return false;
				}

			}
		}
	// will happen if the only material left is piece other than king
	// or that only kings are left 
	return true;
}

/*
	-- get the sqr to the king square of current player
	to the ref square it gets --
	* input: board, negetive turn ( the other player's king )
	* output : king square
*/
Square move::getKingSquare(const Board& board, const color& turn)
{

	Square currSqr;

	char king = turn == color::black ? 'k' : 'K';


	for (int y = 0; y < BOARD_SIDE; ++y)
		for (int x = 0; x < BOARD_SIDE; ++x)
		{
			currSqr.setPosition(x, y);

			if (board.getSqrType(currSqr) == king)
				return currSqr;
		}

	
	return Square::empty(); // never gets here. 

}

/*
	-- find if current move is legal & if there's check 
	(helper function to check mate) -- 
	*input:  source square, dest square , board
	*output: checkmate or not

*/
bool move::validMovement(const Square& src, const Square& dest,
	const Board& board)
{

	if (board.getSqr(src)->isMoveValid(src, dest, board))
		if (valid::notSelfCheck(src, dest, board))
			return true;

	return false;

}


