#pragma once

#include "Piece.h"
#include "stdafx.h"


enum class color{ empty = -1 , white, black };

constexpr int BOARD_SIDE = 8;

typedef std::array <std::array<Piece*, BOARD_SIDE>, BOARD_SIDE> boardStruct;



class Piece;

class Board
{

public:

	//Constructors
	Board(boardStruct board);
	~Board();

	
	//setters
	void setBoard(boardStruct board);
	void setMove(const Square& src, const Square& dest, const bool free = true);
	void setPiece(const Square& sqr, Piece* piece);


	//getters
	Piece* getSqr(const Square& place) const;
	char getSqrType(const Square& sqr, const bool retSymbols = false) const;
	color getPieceColor(const Square& sqr) const;

	//methods
	void show();
	void clear();

private:
	//field
	boardStruct _board;

};

