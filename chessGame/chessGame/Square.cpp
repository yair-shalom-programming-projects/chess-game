#include "Square.h"


using std::string;

constexpr int FIRST_INDEX = 8;
constexpr int TWO = 2;




//? C'tors ?//

Square::Square() : 
	_x(0), _y(0)
{
}
Square::Square(const string& movement)
{

	// index (0,0) is (a,8) so sub 8 from given Square x
	_x = movement[0] - 'a';
	_y = FIRST_INDEX - (movement[1] - '0');

}
Square::Square(const int& x, const int& y) :
	_x(x), _y(y)
{
}




//? Getters ?//

/* -- Get x -- 
* output: x value */
int Square::getX() const
{
	return _x;
}

/* -- Get y -- 
* output: y value */
int Square::getY() const
{
	return _y;
}


//? Setters ?//

/*
	-- Set new position of a square --
	* input: new y , new x
*/
void Square::setPosition(const int& x, const int& y)
{
	_x = x, _y = y;
}

/* -- set X -- */
void Square::setX(const int& newX)
{
	_x = newX;
}
/* -- set Y -- */
void Square::setY(const int& newY)
{
	_y = newY;
}


//? Methods ?//

/* -- calculate the distance between two squares --
* input: other square
* output: return distance */
double Square::Calc_Distance(const Square& other) const
{
	// Distance Formula
	int x = other.getX() - _x;
	int y = other.getY() - _y;

	return sqrt( pow(x, TWO) + pow(y, TWO));
}

/* -- return empty sqr -- */
Square Square::empty()
{
	return Square(-1, -1);
}


//? Operators ?//


/* -- Implementation of the operator !=  -- */
bool Square::operator!=(const Square& other) const
{
	return ! (*this == other);
}

/* -- Implementation of the operator ==  -- */
bool Square::operator==(const Square& other) const
{
	return _x == other.getX() && _y == other.getY();
}

