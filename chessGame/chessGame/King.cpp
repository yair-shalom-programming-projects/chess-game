#include "King.h"
#include "Board.h"
#include "ChessUtils.h"
#include "Rook.h"

using namespace ChessUtils;
using valid::notSelfCheck;


//? Constructors ?//

// -- C'tor -- //
King::King(const char& type) :
	Piece(type)
{
}




//? Getters ?//

/* -- is first move -- */
bool King::isFirstMove() const
{
	return _firstMove;
}







//? Methods ?//


/*
	-- check if King's move is valid -- 
	* input: current Square, dest Square, board
	* output: bool - valid move or not  
*/
bool King::isMoveValid(const Square& src, const Square& dest,
	const Board& board)
{
	/*
		-- KING MOVE RULES --
		1. one step at a time*
		2. move diagonally or in a line
		3. castling - "switch" with the rook
	*/


	double distance = src.Calc_Distance(dest);



	// if in the dest square there's a ally piece
	if (board.getPieceColor(dest) == _turn)
	{
		// find if there's castling
		if (castling(src, dest, board))
			return true;
	}
	
	// if dest isn't an ally, move can only be made one step at a time 
	else if (distance == DIAGONAL_STEP || distance == STRAIGHT_STEP)
		return true;


	return false;
}


/*
	-- find if there's a castling --
	* input: movement (src, dest Squares), board
*/
bool King::castling(const Square& src, const Square& dest, 
	const Board& constBoard)
{

	/*
		CONDITIONS FOR CASTLING:
		1. dest Piece can only be a rook 
		2. it is the King and the choosen Rook's first move
		3. there's no check on the King
		4. there's no check at the Rook (the king new place)
		5. the King is not stepping on a threaten square
		6. there's no piece sepereting the King and the Rook
	*/


	//in castling, the king always move 2 steps
	const int kingSteps = 2;

	Board& board = const_cast<Board&>(constBoard);
	
	Piece* rook;

	

	// find if the dest Piece is rook
	if ( (rook = board.getSqr(dest)) && tolower(rook->getType()) == 'r' )
	{
		
		Piece* king = board.getSqr(src);


		// find if this is the king and rook's first move
		if ( ((King*)king)->isFirstMove() && ((Rook*)rook)->isFirstMove() )

			// find if there's curently a a check on this player
			if ( !_isCheck )
			{
				// Where var tells in witch direction the king will move 
				// ( -1 for left and 1 for right )
				int Where = src.getX() > dest.getX() ? -1 : 1;

				if (moveKingStepByStep(src, dest, board, kingSteps, Where))
				{
				
					// move Rook to the King's right/left Square
					Square& newSrc = const_cast<Square&>(src);
					Square& newDest = const_cast<Square&>(dest);
					Square helper = dest;
					
					newDest = Square(src.getX() + Where, src.getY());
					newSrc = helper;
					
					return true;
				}
			}
	}

	return false;
}


/* 
	-- move king step by step (helper function to castling) --
	* input: dest sqr, board, steps, where to move (left or right) 
	*/
bool King::moveKingStepByStep(const Square& src, const Square& dest,
	Board& board, const int& steps, const int& whereToMove)
{

	Square tempSrc;
	Square tempDest = src;


	try
	{ 

		// find if the king's path is clear
		for (int i = 0; i < steps; ++i)
		{


			tempSrc = tempDest;
			tempDest.setX(tempDest.getX() + whereToMove);

			// if King steps on none-empty OR on threatened sqr - invalid castling
			if (board.getSqr(tempDest) || !notSelfCheck(tempSrc, tempDest, board))
				throw false;
			

			// move the King one step 
			board.setMove(tempSrc, tempDest);


		}

		return true;

	}
	// if king steps are invalid - king go back to his sqr and return false
	catch (...)
	{
		if (tempSrc != src)
			board.setMove(tempSrc, src);
		return false;
	}

}
