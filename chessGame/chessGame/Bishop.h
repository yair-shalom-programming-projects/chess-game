#pragma once

#include "Square.h"
#include "Blockable.h"


class Bishop : public Blockable
{
public:

	// constructors
	Bishop(const char& type);
	virtual ~Bishop() = default;

	// methods
	virtual bool isMoveValid(const Square& curSqr,
		const Square& destSqr, const Board& board) override;
	virtual void setThreatenSquares(const Square& currSqr , 
		const Board& board);



};

