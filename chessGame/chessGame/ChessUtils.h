#pragma once
#include "Square.h"
#include "Board.h"


namespace ChessUtils
{

	// player's turn
	__declspec(selectany) color _turn = color::white;
	
	// if there's a check currently on a player
	__declspec(selectany) bool _isCheck = false;

	// will hold en Passant Square
	__declspec(selectany) Square _enPassantSqr;

	//counter for the amount of pieces that got eaten
	// will help with insufficent material
	__declspec(selectany) unsigned int _eatCounter = 0;
	

	color otherTurn();
	void switchTurn();


	void updateEnPassant(const Square& dest, const Board& board);



	// validate player's movement
	namespace valid
	{
		/* Checks if the squares are in bounds */
		bool squaresInBoard(const Square& src, const Square& dest,
			const Board& board);

		/* Checks if squares are equal */
		bool notEqualSquares(const Square& src, const Square& dest,
			const Board& board);

		/* Checks if the src square actually has a piece of the player*/
		bool playerSrcSquare(const Square& src, const Square& dest,
			const Board& board);

		/* Checks if in the dest square doesnt has an ally piece*/
		bool playerDestSquare(const Square& src, const Square& dest,
			const Board& board);

		/* Checks the movement if the movement mathces the piece  */
		bool badPieceMove(const Square& src, const Square& dest,
			const Board& board);

		/* Checks if the move doesn't cause for a check on himself*/
		bool notSelfCheck(const Square& src, const Square& dest,
			const Board& board );
	}


	// to find if player's movement is special
	namespace move
	{
		/* find if the move is Checked */
		bool isCheck(const Square& src, const Square& dest,
			const Board& board);
		/* find if the move Checkmated */
		bool checkMate(const Square& dest, const Board& board);

		/* find if the move caused a draw*/
		bool isDraw(const Piece& piece, const Board& board);


		// Helper functions
		bool canKingMove(const Square& king, const Board& board);
		bool isCheckBlockable(const Square& king, const Square& dest,
			const Board& board);
		
		/*checks if there is an available move*/
		bool availableMove(const Board& board);
		
		/* checks if there is a case of insufficient material */
		bool insufficientMaterial(const Piece& piece, const Board& board);

		Square getKingSquare(const Board& board, const color& turn);
		bool validMovement(const Square& src, const Square& dest,
			const Board& board);
	};

};

