#pragma once

#include "Square.h"
#include "Board.h" 
#include "Pipe.h"


constexpr int MAX_MSG_SIZE = 66;
constexpr const char* BOARD_FIRST_STATE = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0";




enum class responseCode {	NONE = -1,
							REGULAR, CHECK, BAD_SRC, BAD_DEST, SELF_CHECK,
							BAD_RANGE, BAD_MOVE, EQUAL_SQUARES, CHECKMATE , DRAW  };




class Chess
{

public:

	// Constructors
	Chess();
	~Chess() = default;

	

	// Connection methods
	bool connect();
	std::string prepareMsgToGraphics(const responseCode& resCode);

	// Game methods
	void play();
	bool handleTurn(const std::string& movement, responseCode& resCode);
	void validMove(const Square& src, const Square& dest, 
		responseCode& resCode);
	void pawnPromotion(const Square& dest);
	void restartGame();

	// Static Methods
	static void setAllThreatenSqrs(const Board& board);
	

private:

	//fields
	Board _board;
	Pipe _p;

};