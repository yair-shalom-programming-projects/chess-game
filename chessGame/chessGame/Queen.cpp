#include "Queen.h"




//? Constructors ?//


// -- C'tor -- //
Queen::Queen(const char& type) :
	Blockable(type)
{
	
}




//? Methods ?//


/*
	-- check if Queen's move is valid --
	* input: current Square, dest Square, board
	* output: bool - valid move or not
*/
bool Queen::isMoveValid(const Square& curSqr, const Square& destSqr,
													const Board& board)
{

	/*
		-- QUEEN MOVE RULES --
		1. move diagonally or in a line
	*/


	// Find if the movement is a valid Queen movemnet
	for (int i = 0; i < _threatenSquares.size(); ++i)
	{
		if (destSqr == _threatenSquares[i])
			return true;
	}

	return false;
}




/*
	-- Sets the squares that the queen threates--
	* input: current Square, board
*/
void Queen::setThreatenSquares(const Square& currSqr, const Board& board)
{
	_threatenSquares.resize(0);
	_threatenSquares.push_back(currSqr);

	for (int adderX = -1 ; adderX <= 1; ++adderX)
	{
		for (int adderY = -1; adderY <= 1; ++adderY)
		{
			checkBlocked(currSqr , adderX, adderY , board );
		}
	}
}
