#pragma once

#include "Square.h"
#include "Piece.h"




class King : public Piece
{

public:

	//constructors
	King(const char& type);
	virtual ~King() = default;
	
	//getters
	bool isFirstMove() const;
	
	//methods
	virtual bool isMoveValid(const Square& curSqr,
		const Square& destSqr, const Board& board) override;

	bool castling(const Square& src, const Square& dest,
		const Board& constBoard);

	bool moveKingStepByStep(const Square& src, const Square& dest,
		Board& board, const int& steps, const int& Where);


};

