#include "Bishop.h"
#include "Board.h"



//? Constructor ?//


/* -- C'tor -- */
Bishop::Bishop(const char& type):
	Blockable(type)
{
}



//? Methods ?//


/*
	-- check if Bishop's move is valid --
	* input: current Square, dest Square, board
	* output: bool - valid move or not
*/
bool Bishop::isMoveValid(const Square& curSqr, const Square& destSqr,
														const Board& board)
{
	/*
		-- BISHOP MOVE RULES --
		1. move diagonally only
	*/


	// Find if the Bishop is moving diagonally and if his path is clear
	for (int i = 0; i < _threatenSquares.size(); ++i)
	{
		if (destSqr == _threatenSquares[i])
			return true;
	}


	return false;
}






/*
	-- Sets the squares that the bishop threates --
	* input: current Square, board
*/
void Bishop::setThreatenSquares( const Square& currSqr, const Board& board)
{

	// reset threatenSquare & push back current Bishop's Square
	_threatenSquares.resize(0);
	_threatenSquares.push_back(currSqr);
	
	
	// bishop can move diagonally and his first steps can be: 
	//	(x-1,y-1) & (y+1,x-1) & (y-1,x+1) & (x+1, y+1) 
	for (int x = -1; x < 2; x += 2)
	{
		for (int y = -1 ;  y < 2; y += 2)
		{
			// check when the path is blocked 
			// and adds every valid move option to threatenSqr
			checkBlocked( currSqr,x , y, board );
		}
	}
}

