#pragma once

#include "Square.h"
#include "stdafx.h"


// valid diagonal and straight distance movements for the King
constexpr double DIAGONAL_STEP = 1.4142135623730951;// sqrt(2)
constexpr double STRAIGHT_STEP = 1;// sqrt(1)


class Board;

class Piece
{

public:

	//Constructors
	Piece(const char& newType);
	virtual ~Piece() = default;


	//Methods
	virtual bool isMoveValid(const Square& curSqr, 
							const Square& destSqr, const Board& board) = 0;	


	//Setters
	virtual void setThreatenSquares(const Square& currSqr,
		const Board& board) ;

	virtual void updateFirstMove();
	
	//getters
	virtual char getType() const;
	virtual Square getThreatendSqr(int index) const;


protected:
	bool _firstMove;

private:

	char _type;

};

