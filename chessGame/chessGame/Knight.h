#pragma once

#include "Piece.h"
#include "Square.h"
#include "Board.h"


constexpr double KNIGHT_STEP = 2.2360679774997898; // sqrt(5)


class Knight: public Piece
{

public:

	//constructors
	Knight(const char& type);
	virtual ~Knight() = default;
	

	//methods
	virtual bool isMoveValid(const Square& curSqr, const Square& destSqr,
		const Board& board) override;
	
};

