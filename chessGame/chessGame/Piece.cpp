#include "Piece.h"

//? Constructors ?//

/* -- C'tor -- */
Piece::Piece(const char& newType) :
	_type(newType), _firstMove(true)
{
}

//?  Getters  ?//







// -- Getter for type -- //
char Piece::getType() const
{
	return _type;
}





/* -- Sets the _firstMove field to false -- */
void Piece::updateFirstMove()
{
	_firstMove = false;
}



/*
	Both of the functions below dont have any implementation the only reason
	we have those function is so we can use them in queen , rook and bishop
	we need them to first of all check if the move is valid for the any kind 
	of piece and second it makes it way easier to check if any of the pieces
	can block a check in the checkmate functions , the reason that in king,
	knight and pawn there is no implementation is becuase there check cant be
	blocked and king cant even do a check and the implementation of the validation
	of their move is way easier without this methods and field.
*/
void Piece::setThreatenSquares(const Square& currSqr, const Board& board)
{
}


Square Piece::getThreatendSqr(int index) const
{
	return Square::empty();
}





