#include "Pawn.h"
#include "ChessUtils.h"

using namespace ChessUtils;

//? Constructors ?//

/* -- C'tor -- */
Pawn::Pawn(const char& type):
	Piece(type) 
{
}





//?	Methodes ?//
/*

	-- check if the Pawn movement is valid --
	* input: src Square, dest Square, board
	* output: bool - valid move or not 
*/
bool Pawn::isMoveValid(const Square& src, const Square& dest,
	const Board& board)
{
	/*
		-- PAWN MOVE RULES --
		1.	move 1 step foward (in his first move, pawn can move 2 steps).
		2.	eat one step diagonaly (foward) or eat En-Passant.

	*/

	double distance = src.Calc_Distance(dest);
	
	

	// find if direction of the move is even valid - always foward
	if (
		( _turn == color::white && dest.getY() < src.getY() )
			||
		( _turn == color::black && dest.getY() > src.getY() )
	)
	{

		// find if move/eat valid
		if (

			isEatValid(src, dest, board)
				||
			distance == STRAIGHT_STEP && !board.getSqr(dest)
				||
			distance == STRAIGHT_STEP * 2 && !board.getSqr(dest) && _firstMove
		)
		{
			
			// if this is the Pawn's first move and he did double move
			// he's the new en Passant!
			if ( _firstMove && STRAIGHT_STEP * 2 == distance )
				_enPassantSqr = dest;
				
			return true;
		}
		
	}
	return false;
}

/*
	-- check if the pawn's eat is valid --
	* input: src square, dest square, board
	* output: bool - valid eat or not
*/
bool Pawn::isEatValid(const Square& src, const Square& dest, 
	const Board& board) const
{
	
	Board& newBoard = const_cast<Board&>(board);
	double distance = src.Calc_Distance(dest);


	// pawn eats in diagonal step
	if ( distance == DIAGONAL_STEP )
	{

		// 1 - if an enemy piece in dest square
		if (board.getSqr(dest))
			return true;


		// 2 - if enemy is En Passant 
		Square sqr = _turn == color::black 
									?
									Square(dest.getX(), dest.getY() - 1)
										:
									Square(dest.getX(), dest.getY() + 1);

		
		if ( sqr == _enPassantSqr )
		{

			_enPassantSqr = Square::empty();
			newBoard.setPiece(sqr, nullptr);
			
			return true;
		
		}
	}

	return false;
}



