

#include "Board.h"
#include "Bishop.h"
#include "King.h"
#include "Queen.h"
#include "Pawn.h"
#include "Knight.h"
#include "Rook.h"


using std::map;
using std::string;
using std::array;
using std::cout;
using std::endl;




//? Constructors ?//


/* -- C'tor -- */
Board::Board(boardStruct board) :
    _board(board)
{
}


/* -- D'tor -- */
Board::~Board()
{
    clear();
}



//? Setters ?//

/*
    -- set move at board --
    * input: src square, dst square
*/
void Board::setMove(const Square& src, const Square& dest, 
    const bool free)
{
    
    Piece* destVal = _board[dest.getY()][dest.getX()];
    

    //if the square isn't empty & free is true - delete it 
    if (destVal && free)
        delete destVal;
  
    
    _board[dest.getY()][dest.getX()] = _board[src.getY()][src.getX()];
    _board[src.getY()][src.getX()] = nullptr;
    



}
/*
    -- set new board --
    * input: board array
*/
void Board::setBoard(boardStruct board)
{
    clear();
    _board = board;
}
/*
    -- Sets a new piece in a wanted square
    * input: the square and the piece
*/
void Board::setPiece(const Square& sqr, Piece* piece)
{

    Piece* pSqr;


    if (pSqr = _board[sqr.getY()][sqr.getX()])
    {
        delete pSqr;
        pSqr = nullptr;
    }

    _board[sqr.getY()][sqr.getX()] = piece;

}



//? Getters ?//

/*
    -- get square's piece, if sqrs are out of bounds throws exception --
    * input: square
    * output: piece 
*/
Piece* Board::getSqr(const Square& place) const
{

    int x = place.getX();
    int y = place.getY();


    if ( x < 0 || x > 7 || y < 0 || y > 7 )
        throw std::exception("Indexes are out of bounds");

    return _board[place.getY()][place.getX()];

}

/*
    -- get the type of the given square --
    * input: sqr, if return NULL or '#' at the empty squares
    * output: the type or if the sqr empty, NULL/#
*/
char Board::getSqrType(const Square& sqr, const bool retSymbols) const
{

    Piece* p = getSqr(sqr);
    
    // if piece is not empty - return type 
    if (p)
        return p->getType();
    else if (retSymbols)
        return '#';
    else
        return NULL;
}


/* -- Returns the type of the color of the piece in a square --
    *input: square
    *output: the color of the Piece in the square if the square is empty
             returns the type::empty
*/
color Board::getPieceColor(const Square& sqr) const
{
    char type = getSqrType(sqr);
    if (!type)
        return color::empty;

    if (islower(type))
        return color::black;

    return color::white;
}



//? Methods ?//

/* -- show board to cmd -- */
void Board::show()
{
    Square currSqr;


    cout << "---------------------------------- " << endl;

    for (int y = 0; y < BOARD_SIDE; ++y)
    {
        cout << "| ";

        for (int x = 0; x < BOARD_SIDE; ++x)
        {

            currSqr.setPosition(x, y);

            cout << " " << getSqrType(currSqr) << " |";
        
        }
        cout << endl << "----------------------------------" << endl;
    }
    
}
/* -- clear Board -- */
void Board::clear()
{

    for (int y = 0; y < BOARD_SIDE; ++y)
    {
        for (int x = 0; x < BOARD_SIDE; ++x)
        {
            delete _board[y][x];
            _board[y][x] = nullptr;
        }
    }

}

